vim9script

setlocal tw=80 ts=4 sw=4 st=4 et
setlocal cinoptions=f1s,{1s

legacy let c_gnu		= 1
legacy let c_syntax_for_h	= 1

