vim9script

#if exists("c_mg")
  syntax keyword cConstant     __mg_host_byte_order_be
  syntax keyword cConstant     __mg_host_byte_order_le
  syntax keyword cOperator     __me_error_enumerator
  syntax keyword cOperator     __mg_count_of
  syntax keyword cOperator     __mg_static_assert
  syntax keyword cStatement    __mg_extern_c_begin
  syntax keyword cStatement    __mg_extern_c_end
  syntax keyword cStatement    __mg_forward_decl_struct
  syntax keyword cStatement    __mg_trap
  syntax keyword cStatement    __mg_unused
  syntax keyword cStorageClass __mg_always_inline
  syntax keyword cStorageClass __mg_deprecated
  syntax keyword cStorageClass __mg_format_arg
  syntax keyword cStorageClass __mg_inline
  syntax keyword cStorageClass __mg_nodiscard
  syntax keyword cStorageClass __mg_noreturn
  syntax keyword cStorageClass __mg_printf
  syntax keyword cStorageClass __mg_scanf

  syntax match cStorageClass display "__mg_\w\+_export"
#endif

