Vim Configuration
-----------------

0. Ensure you have Vim 9.0+.
1. Clone the repository to the ~/.vim directory.
2. ln -s ~/.vim/init.vim ~/.vimrc
3. cd ~/.vim && git submodule init
